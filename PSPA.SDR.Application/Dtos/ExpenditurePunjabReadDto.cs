﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PSPA.SDR.Application.Dtos
{
    public class ExpenditurePunjabReadDto
    {
        public int Id { get; set; }
        public string DeptId { get; set; }
        public string DeptName { get; set; }
        public string ExpFrom2012To13 { get; set; }
        public string ProgramId { get; set; }
        public string ProgramName { get; set; }
        public string ExpFrom2013To14 { get; set; }
        public string ExpFrom2014To15 { get; set; }
        public string ExpFrom2015To16 { get; set; }
        public string ExpFrom2016To17 { get; set; }
        public string ExpFrom2017To18 { get; set; }
        public string ExpTotal { get; set; }
    }
}
