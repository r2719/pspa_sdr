﻿using Microsoft.EntityFrameworkCore;
using PSPA.SDR.Application.Models.DomainModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PSPA.SDR.Application.Data
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> opt) : base(opt)
        {

        }

        public DbSet<ExpenditurePunjab> expenditurePunjabs { get; set; }
        public DbSet<Sdr> Sdrs { get; set; }
    }
}
