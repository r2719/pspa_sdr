﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using PSPA.SDR.Application.Models;
using PSPA.SDR.Application.Models.DomainModel;
using PSPA.SDR.Application.Models.DropdownViewModel;
using PSPA.SDR.Application.Models.ViewModel;
using PSPA.SDR.Application.Repository;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace PSPA.SDR.Application.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IExpenditurePunjabRepo _repository;
        private readonly ISdrRepo _sdrRepo;

        public HomeController(ILogger<HomeController> logger, IExpenditurePunjabRepo repository, ISdrRepo sdrRepo)
        {
            _logger = logger;
            _sdrRepo = sdrRepo;
            _repository = repository;
        }

        public async Task<IActionResult> Index()
        {
            ViewBag.departwise= await _sdrRepo.GetAllDepartmentsWiseAsync();
            ViewBag.AllData= await _sdrRepo.GetAllDepartmentsRecords();
           // ViewBag.programs = await _sdrRepo.GetAllProgramsAsync();
            ViewBag.departments = await _sdrRepo.GetAllDepartmentsAsync();
            return View();
        } 
        public async Task<IActionResult> SearchByCNIC()
        {
            return View();
        }  
        public async Task<IActionResult> SearchByCNICSingle(SdrFilterViewModel model)
        {
            var searchData = await _sdrRepo.GetSearchResult(model);
            return Ok(searchData);
        }
        public async Task<IActionResult> Programs()
        {
            ViewBag.departments = await _sdrRepo.GetAllDepartmentsAsync();
            SdrFilterViewModel model = new SdrFilterViewModel();
            model.departmentName = "-1";
            ViewBag.Alldepartments = await _sdrRepo.FilterSdrByIdAndAllAsync(model);
            return View();
        }
        public async Task<IActionResult> ProgramAnalytics()
        {
            ViewBag.programs = await _sdrRepo.GetAllProgramsAsync();
            ViewBag.departments = await _sdrRepo.GetAllDepartmentsAsync();
            return View();
        }
        public async Task<IActionResult> DetailedAnalytics()
        {
            ViewBag.programs = await _sdrRepo.GetAllProgramsAsync();
            ViewBag.departments = await _sdrRepo.GetAllDepartmentsAsync();
            return View();
        }
        public async  Task<IActionResult> SocialProtectionExpenses()
        {
            ViewBag.programs = await _repository.GetProgrammAsync();
            ViewBag.departments = await _repository.GetDepartmentAsync();
            return View();
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        [HttpGet]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> FilterExpenditurePunjab(string departmentId, string programId)
        {
            var model = new ExpenditurePunjabFilterViewModel
            {
                departmentId = departmentId,
                programId = programId
            };
            var response = await _repository.FilterExpenditurePunjabByIdAsync(model);
            return Ok(response);
        }

        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> FilterSdr(SdrFilterViewModel model)
        {
            var response = await _sdrRepo.FilterSdrByIdAsync(model);
            var ActiveInResponce = await _sdrRepo.FilterSdrByIdAndAllAsync(model);
            var FinalResponce = new 
            {
                response= response,
                ActiveInResponce= ActiveInResponce
            };
            return Ok(FinalResponce);
        } 
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> GetAllDeptData()
        {
            var response = await _sdrRepo.GetAllDepartmentsWiseAsync();
            return Ok(response);
        }  
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> FilterProgramm(SdrFilterViewModel model)
        {
            ViewBag.programs = await _sdrRepo.FilterProgrammByIdAsync(model);
            return View();
        }
        [HttpPost]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> ProgramWiseStates(SdrFilterViewModel model)
        {
            var response = await _sdrRepo.ProgramWisestatsByIdAsync(model);
            return Ok(response);
        }

        [HttpGet]
        [IgnoreAntiforgeryToken]
        public async Task<IActionResult> GetExpenditurePunjabDepartmentWise()
        {
          
            var response = await _repository.GetExpenditurePunjabDepartmentWiseAsync();
            return Ok(response);
        }
    }
}
