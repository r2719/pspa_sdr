﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PSPA.SDR.Application.Models.DomainModel;
using PSPA.SDR.Application.Models.ViewModel;
using PSPA.SDR.Application.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PSPA.SDR.Application.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SocialProtectionController : ControllerBase
    {
        private readonly IExpenditurePunjabRepo _repository;

        public SocialProtectionController(IExpenditurePunjabRepo repository)
        {
            _repository = repository; 
        }
        [HttpPost]
        [IgnoreAntiforgeryToken(Order = 2000)]
        public async Task<ActionResult<IEnumerable<ExpenditurePunjab>>> FilterExpenditurePunjabAsync([FromBody]  ExpenditurePunjabFilterViewModel model)
        {
            var response = await _repository.FilterExpenditurePunjabByIdAsync(model);
            return Ok(response);
        }
    }
}
