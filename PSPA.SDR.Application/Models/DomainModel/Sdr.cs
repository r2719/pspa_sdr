﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PSPA.SDR.Application.Models.DomainModel
{
    [Table("SDR_Table")]
    public class Sdr
    {
        [Column("ID")]
        public decimal Id { get; set; }
        [Column("HH_ID")]
        public double? HHId { get; set; }
        [Column("CITIZEN_NO")]
        public double? CitizenNo { get; set; }
        [Column("CNIC_Check")]
        public double? CnicCheck { get; set; }
        [Column("HHR_ID")]
        public double? HHRId { get; set; }
        [Column("NAME")]
        public string Name { get; set; }
        [Column("AGE")]
        public double? Age { get; set; }
        [Column("DOB")]
        public DateTime Dob { get; set; }
        [Column("Calculated_Age")]
        public double? CalculatedAge { get; set; }
        [Column("GENDER")]
        public string Gender { get; set; }
        [Column("MARITAL_STATUS")]
        public string MaritalStatus { get; set; }
        [Column("BISP_BENEFICIARY_STATUS")]
        public string BISPBeneficiaryStatus { get; set; }
        [Column("HEAD_RELATION")]
        public string HeadRelation { get; set; }
        [Column("ADDRESS")]
        public string Address { get; set; }
        [Column("DISTRICT")]
        public string District { get; set; }
        [Column("TEHSIL")]
        public string Tehsil { get; set; }
        [Column("UNIONCOUNCIL")]
        public string Unioncouncil { get; set; }
        [Column("POSTOFFICE")]
        public string PostOffice { get; set; }
        [Column("VILLAGE")]
        public string Village { get; set; }
        [Column("CONTACT_NO")]
        public string ContactNo { get; set; }
        [Column("DISABILITY")]
        public string Disability { get; set; }
        [Column("EMP_CATEGORY")]
        public string EmpCategory { get; set; }
        [Column("EDUCATION_STATUS")]
        public string EductionStatus { get; set; }
        [Column("EDUCATION_LEVEL")]
        public string EductionLevel { get; set; }
        [Column("NO_ROOMS")]
        public double? NoRooms { get; set; }
        [Column("TOILET_KIND")]
        public string TotalKind { get; set; }
        [Column("AIRCOOLER_P")]
        public string AirCooler { get; set; }
        [Column("TV_P")]
        public string Tv { get; set; }
        [Column("FREEZER_P")]
        public string Freezer { get; set; }
        [Column("AIRCONDITIONER_P")]
        public string AirCondittioner { get; set; }
        [Column("WASHINGMACHINE_P")]
        public string WashingManchine { get; set; }
        [Column("HEATER_P")]
        public string Heater { get; set; }
        [Column("COOKINGRANGE_P")]
        public string CookingRange { get; set; }
        [Column("MICROWAVEOVEN_P")]
        public string MicrowaveOven { get; set; }
        [Column("BULL_P")]
        public string Bull { get; set; }
        [Column("SHEEP_P")]
        public string Sheep { get; set; }
        [Column("BUFFALO_P")]
        public string Buffalo { get; set; }
        [Column("COW_P")]
        public string Cow { get; set; }
        [Column("GOAT_P")]
        public string goat { get; set; }
        [Column("LAND_AREA")]
        public double? LandArea { get; set; }
        [Column("LAND_UNIT")]
        public string LandUnit { get; set; }
        [Column("PMT_SCORE")]
        public double? PmtScore { get; set; }
        [Column("INSERTION_TIMESTAMP")]
        public string InsertionTimeStamp { get; set; }
        [Column("Record_Age")]
        public double? RecordAge { get; set; }
        [Column("BISP_Excluded_HH")]
        public string BISPExcludedHH { get; set; }
        [Column("CNIC")]
        public string Cnic { get; set; }
        [Column("HH_with_CNIC")]
        public int HHWithCNIC { get; set; }
        [Column("BISP_Beneficiary_Count")]
        public int BISPBeneficiaryCount { get; set; }
        [Column("HH_SIZE")]
        public int HHSize { get; set; }
        [Column("Child_Count")]
        public int ChildCount { get; set; }
        [Column("female_count")]
        public int FemaleCount { get; set; }
        [Column("Father_Name")]
        public string FatherName { get; set; }
        [Column("CNIC_Father")]
        public string CNICFather { get; set; }
        [Column("Mother_Name")]
        public string MotherName { get; set; }
        [Column("CNIC_Mother")]
        public string CNICMother { get; set; }
        [Column("Domicile")]
        public string Domicile { get; set; }
        [Column("Date_data_collection")]
        public DateTime DateDataCollection { get; set; }
        [Column("Department_Name")]
        public string DepartmentName { get; set; }
        [Column("Program_Name")]
        public string ProgramName { get; set; }
        [Column("Is_Beneficiary_Active")]
        public string IsBeneficiaryActive { get; set; }
        [Column("Date_of_death")]
        public DateTime DateOfDeath { get; set; }
        [Column("Occupation")]
        public string Occupation { get; set; }
        [Column("CONTACT_NO2")]
        public string ContactNo2 { get; set; }
        [Column("Division")]
        public string Division { get; set; }
        [Column("Religion")]
        public string Religion { get; set; }
        [Column("Employment_Status")]
        public string EmploymentStatus { get; set; }
        [Column("Spouse_Name")]
        public string SpouseName { get; set; }
        [Column("CNIC_Spouse")]
        public string CNICSpouse { get; set; }
        [Column("Guardian_Name")]
        public string GuardianName { get; set; }
        [Column("CNIC_Guardian")]
        public string CNICGuardian { get; set; }
        [Column("Dept_ID")]
        public int DeptID { get; set; }
        [Column("Program_ID")]
        public int ProgramId { get; set; }
        [Column("Head_Name")]
        public string HeadName { get; set; }
        [Column("CNIC_head")]
        public string CNICHead { get; set; }
        [Column("Skills")]
        public string Skills { get; set; }
        [Column("IID")]
        public int IId { get; set; }
        [Column("Zakat_Category")]
        public string ZakatCategory { get; set; }
        [Column("Registration_Date")]
        public DateTime RegistrationDate { get; set; }
        [Column("District_ID")]
        public int DistrictID { get; set; }
        [Column("Tehsil_ID")]
        public int TehsilID { get; set; }
        [Column("Total_CR")]
        public string TotalCR { get; set; }
        [Column("LAST_DEPOSIT_DATE")]
        public DateTime LASTDEPOSITDATE { get; set; }
        [Column("LAST_WITHDRAWAL_DATE")]
        public DateTime LASTWITHDRAWALDATE { get; set; }
        [Column("System_Type")]
        public int SystemType { get; set; }
    }
}
