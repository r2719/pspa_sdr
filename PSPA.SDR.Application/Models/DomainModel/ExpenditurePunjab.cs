﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace PSPA.SDR.Application.Models.DomainModel
{
    [Table("SP_Expenditure_Punjab_2012_18_F")]
    public class ExpenditurePunjab
    {
        [Column("Id")]
        public int Id { get; set; }
        [Column("Dept_ID")]
        public string DeptId { get; set; }
        [Column("Dept_name")]
        public string DeptName { get; set; }
        [Column("Exp_2012_13")]
        public string ExpFrom2012To13 { get; set; }
        [Column("Program_ID")]
        public string ProgramId { get; set; }
        [Column("Program_Name")]
        public string ProgramName { get; set; }
        [Column("Exp_2013_14")]
        public string ExpFrom2013To14 { get; set; }
        [Column("Exp_2014_15")]
        public string ExpFrom2014To15 { get; set; }
        [Column("Exp_2015_16")]
        public string ExpFrom2015To16 { get; set; }
        [Column("Exp_2016_17")]
        public string ExpFrom2016To17 { get; set; }
        [Column("Exp_2017_18")]
        public string ExpFrom2017To18 { get; set; }
        [Column("Exp_Total_2012_18")]
        public string ExpTotal { get; set; }
    }
}
