﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PSPA.SDR.Application.Models.DropdownViewModel
{
    public class Countries
    {
        public string Text { get; set; }
        public string Value { get; set; }
        public IEnumerable<Countries> CountriesList()
        {
            List<Countries> country = new List<Countries>();
            country.Add(new Countries { Text = "Australia", Value = "AU" });
            country.Add(new Countries { Text = "Bermuda", Value = "BM" });
            country.Add(new Countries { Text = "Canada", Value = "CA" });
            country.Add(new Countries { Text = "Cameroon", Value = "CM" });
            country.Add(new Countries { Text = "Denmark", Value = "DK" });
            country.Add(new Countries { Text = "France", Value = "FR" });
            country.Add(new Countries { Text = "Finland", Value = "FI" });
            country.Add(new Countries { Text = "Germany", Value = "DE" });
            country.Add(new Countries { Text = "Greenland", Value = "GL" });
            country.Add(new Countries { Text = "Hong Kong", Value = "HK" });
            country.Add(new Countries { Text = "India", Value = "IN" });
            country.Add(new Countries { Text = "Italy", Value = "IT" });
            country.Add(new Countries { Text = "Japan", Value = "JP" });
            country.Add(new Countries { Text = "Mexico", Value = "MX" });
            country.Add(new Countries { Text = "Norway", Value = "NO" });
            country.Add(new Countries { Text = "Poland", Value = "PL" });
            country.Add(new Countries { Text = "Switzerland", Value = "CH" });
            country.Add(new Countries { Text = "United Kingdom", Value = "GB" });
            country.Add(new Countries { Text = "United States", Value = "US" });
            return country;
        }
    }
}
