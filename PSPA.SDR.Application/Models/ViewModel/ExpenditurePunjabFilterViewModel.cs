﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PSPA.SDR.Application.Models.ViewModel
{
    public class SdrFilterViewModel
    {
        public string departmentName { get; set; }
        public string programName { get; set; }
        public string program { get; set; }
        public string Cnic { get; set; }
    }
}
