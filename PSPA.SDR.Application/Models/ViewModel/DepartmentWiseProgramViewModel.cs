﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PSPA.SDR.Application.Models.ViewModel
{
    public class DepartmentWiseProgramViewModel
    {
        public int SrNo { get; set; }
        public string ProgramName { get; set; }
        public string DepartmentName { get; set; }
        public int Cases { get; set; }
        public int Beneficiaries { get; set; }
        public int Total { get; set; }
    }
}
