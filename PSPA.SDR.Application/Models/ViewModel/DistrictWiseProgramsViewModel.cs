﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PSPA.SDR.Application.Models.ViewModel
{
    public class DistrictWiseProgramsViewModel
    {
        public int SrNo { get; set; }
        public string District { get; set; }
        public string ProgramName { get; set; }
        public int RegisteredPersons { get; set; }
        public int Beneficiaries { get; set; }
        public int Male { get; set; }
        public int Female { get; set; }
        public int Trans { get; set; }
        public string DisabilityName { get; set; }

    }
}
