﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PSPA.SDR.Application.Models.ViewModel
{
    public class ProgramWisestatsViewModel
    {
        public ProgramWisestatsViewModel()
        {
        }
        public IEnumerable<DistrictWiseProgramsViewModel> districtWisePrograms { get; set; }
        public IEnumerable<DepartmentWiseProgramViewModel> departmentWisePrograms { get; set; }
    }
}
