﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PSPA.SDR.Application.Models.ViewModel
{
    public class DepartmentWiseExpenseViewModel
    {
        public string DepartmentName { get; set; }
        public string ProgramName { get; set; }
        public double total { get; set; }
    }
}
