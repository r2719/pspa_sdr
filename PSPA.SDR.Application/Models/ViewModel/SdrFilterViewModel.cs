﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PSPA.SDR.Application.Models.ViewModel
{
    public class ExpenditurePunjabFilterViewModel
    {
        public string departmentId { get; set; }
        public string programId { get; set; }
    }
}
