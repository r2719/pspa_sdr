﻿using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using PSPA.SDR.Application.Data;
using PSPA.SDR.Application.Models.DomainModel;
using PSPA.SDR.Application.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Data.SqlClient;
using Dapper;
using System.Configuration;
using Microsoft.Extensions.Configuration;
using PSPA.SDR.Application.Extensions;

namespace PSPA.SDR.Application.Repository
{
    public class SdrRepo : ISdrRepo
    {
        private readonly ApplicationDbContext _context;
        private readonly IConfiguration _configuration;
        public SdrRepo(ApplicationDbContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }
        public async Task<IEnumerable<DepartmentWiseProgramViewModel>> FilterSdrByIdAndAllAsync(SdrFilterViewModel model)
        {
            try
            {
                    string DbConnectionString = this._configuration.GetConnectionString("DefaultConnection");
                    var p = new DynamicParameters();
                    p.Add("@DeprtName", model.departmentName);
                    IEnumerable<DepartmentWiseProgramViewModel> departmentWiseProgramList;
                    using (IDbConnection db = new SqlConnection(DbConnectionString))
                    {
                        string readSp = "dbo.DepartmentWiseProgramActiveInActive";
                        departmentWiseProgramList = await db.QueryAsync<DepartmentWiseProgramViewModel>(readSp, p, commandType: CommandType.StoredProcedure);
                    }

                    return departmentWiseProgramList;
            }
            catch(Exception ex)
            {

            }
            return new List<DepartmentWiseProgramViewModel>();
        }  
        public async Task<IEnumerable<DepartmentWiseProgramViewModel>> FilterSdrByIdAsync(SdrFilterViewModel model)
        {
            try
            {
                if (model.departmentName!=null )
                {
                    string DbConnectionString = this._configuration.GetConnectionString("DefaultConnection");
                    var p = new DynamicParameters();
                    p.Add("@DeprtName", model.departmentName);
                    IEnumerable<DepartmentWiseProgramViewModel> departmentWiseProgramList;
                    using (IDbConnection db = new SqlConnection(DbConnectionString))
                    {
                        string readSp = "dbo.DepartmentWiseProgramRegisterBenifi";
                        departmentWiseProgramList = await db.QueryAsync<DepartmentWiseProgramViewModel>(readSp, p, commandType: CommandType.StoredProcedure);
                    }

                    return departmentWiseProgramList;
                }
                else
                {
                    string DbConnectionString = this._configuration.GetConnectionString("DefaultConnection");
                    var p = new DynamicParameters();
                    p.Add("@DeprtName", model.programName);
                    IEnumerable<DepartmentWiseProgramViewModel> departmentWiseProgramList;
                    using (IDbConnection db = new SqlConnection(DbConnectionString))
                    {
                        string readSp = "dbo.ProgrammWiseProgramRegisterBenifi";
                        departmentWiseProgramList = await db.QueryAsync<DepartmentWiseProgramViewModel>(readSp, p, commandType: CommandType.StoredProcedure);
                    }

                    return departmentWiseProgramList;

                }
            }
            catch(Exception ex)
            {

            }
            return new List<DepartmentWiseProgramViewModel>();
        }
        public async Task<List<SelectListItem>> FilterProgrammByIdAsync(SdrFilterViewModel model)
        {
            try
            {
                var response = await _context.Sdrs.Where(x => x.DepartmentName == model.departmentName).Select(x => new SelectListItem
                {
                    Text = x.ProgramName,
                    Value = x.ProgramName
                }).Distinct().ToListAsync();
                return response;
            }
            catch (Exception)
            {
                throw;
            }
        }
        public async Task<ProgramWisestatsViewModel> ProgramWisestatsByIdAsync(SdrFilterViewModel model)
        {
            try
            {
                var Perameter = model.departmentName == null ? "-1" : model.departmentName;
                
                string DbConnectionString = this._configuration.GetConnectionString("DefaultConnection");
                var p = new DynamicParameters();
                p.Add("@DeptName", Perameter);
                IEnumerable<DepartmentWiseProgramViewModel> departmentWisePrograms;
                using (IDbConnection db = new SqlConnection(DbConnectionString))
                {
                    string readSp = "dbo.GetProgrammsStats";
                    departmentWisePrograms = await db.QueryAsync<DepartmentWiseProgramViewModel>(readSp, p, commandType: CommandType.StoredProcedure);
                }
                var d = new DynamicParameters();
                d.Add("@DeptName", Perameter);
                IEnumerable<DistrictWiseProgramsViewModel> districtWisePrograms;
                using (IDbConnection db = new SqlConnection(DbConnectionString))
                {
                    string readSp = "dbo.GetDistrictWiseStats";
                    districtWisePrograms = await db.QueryAsync<DistrictWiseProgramsViewModel>(readSp, d, commandType: CommandType.StoredProcedure);
                }
                var vm = new ProgramWisestatsViewModel {
                departmentWisePrograms = departmentWisePrograms,
                districtWisePrograms =districtWisePrograms
                };

                return vm;
            }
            catch(Exception ex)
            {

            }
            return new ProgramWisestatsViewModel();
        }

        public async Task<List<SelectListItem>> GetAllDepartmentsAsync()
        {
            try
            {
                string DbConnectionString = this._configuration.GetConnectionString("DefaultConnection");
                List<SelectListItem> selectListItem;
                using (IDbConnection db = new SqlConnection(DbConnectionString))
                {
                    string readSp = "dbo.GetAllDepartments";
                    selectListItem = db.Query<SelectListItem>(readSp, commandType: CommandType.StoredProcedure).ToList();
                }
                return selectListItem;
            }
            catch (Exception)
            {

                throw;
            }
            

        }
        public async Task<List<SelectListItem>> GetAllProgramsAsync()
        {
            try
            {
                string DbConnectionString = this._configuration.GetConnectionString("DefaultConnection");
                List<SelectListItem> selectListItem;
                using (IDbConnection db = new SqlConnection(DbConnectionString))
                {
                    string readSp = "dbo.GetAllProgramms";
                    selectListItem = db.Query<SelectListItem>(readSp, commandType: CommandType.StoredProcedure).ToList();
                }
                return selectListItem;
            }
            catch (Exception)
            {

                throw;
            }
         
        }
        public async Task<List<DepartmentWiseProgramViewModel>> GetAllDepartmentsWiseAsync()
        {
            try
            {
                string DbConnectionString = this._configuration.GetConnectionString("DefaultConnection");
                var p = new DynamicParameters();
                p.Add("@DeprtName", "-1");
                IEnumerable<DepartmentWiseProgramViewModel> finalResult;
                using (IDbConnection db = new SqlConnection(DbConnectionString))
                {
                    string readSp = "dbo.DepartmentWiseProgramRegisterBenifi";
                    finalResult = await db.QueryAsync<DepartmentWiseProgramViewModel>(readSp, p, commandType: CommandType.StoredProcedure);
                }

                return finalResult.ToList();
            }
            catch (Exception)
            {

                throw;
            }
           

        } 
        public async Task<List<Sdr>> GetSearchResult(SdrFilterViewModel model)
        {
            try
            {
                var Perameter = model.Cnic == null ? "-1" : model.Cnic;
                string DbConnectionString = this._configuration.GetConnectionString("DefaultConnection");
                var p = new DynamicParameters();
                p.Add("@CNIC", Perameter);
                IEnumerable<Sdr> responsebenifi;
                using (IDbConnection db = new SqlConnection(DbConnectionString))
                {
                    string readSp = "dbo.GetRecordByCNIC";
                    responsebenifi = await db.QueryAsync<Sdr>(readSp, p, commandType: CommandType.StoredProcedure);
                }

                return responsebenifi.ToList();
            }
            catch (Exception)
            {

                throw;
            }
           
            
        } 
        public async Task<List<DepartmentWiseProgramViewModel>> GetAllDepartmentsRecords()
        {
            try
            {
                string DbConnectionString = this._configuration.GetConnectionString("DefaultConnection");
                IEnumerable<DepartmentWiseProgramViewModel> finalResult;
                using (IDbConnection db = new SqlConnection(DbConnectionString))
                {
                    string readSp = "dbo.GetAllDepartmentsData";
                    finalResult = await db.QueryAsync<DepartmentWiseProgramViewModel>(readSp, commandType: CommandType.StoredProcedure);
                }

                return finalResult.ToList();
            }
            catch (Exception)
            {

                throw;
            }
           
            

        }

        public async Task<IEnumerable<Sdr>> GetAllSdrAsync()
        {
            try
            {
                return await _context.Sdrs.ToListAsync();
            }
            catch (Exception)
            {

                throw;
            }
            
        }
        public async Task<IEnumerable<DepartmentWiseExpenseViewModel>> GetExpenditurePunjabDepartmentWiseAsync()
        {
            try
            {
                var departments = await _context.expenditurePunjabs.Select(x => x.DeptName).Distinct().ToListAsync();
                List<DepartmentWiseExpenseViewModel> departmentwiseList = new List<DepartmentWiseExpenseViewModel>();
                foreach (var department in departments)
                {
                    var total = await _context.expenditurePunjabs.Where(x => x.DeptName == department).SumAsync(s => Convert.ToDouble(s.ExpTotal));

                    var vm = new DepartmentWiseExpenseViewModel
                    {
                        DepartmentName = department,
                        total = total
                    };

                    departmentwiseList.Add(vm);
                }
                return departmentwiseList;
            }
            catch (Exception)
            {

                throw;
            }
   
        }

        public async Task<bool> SaveChangesAsync()
        {
            try
            {
                return (await _context.SaveChangesAsync() >= 0);
            }
            catch (Exception)
            {

                throw;
            }
          
        }
    }
}
