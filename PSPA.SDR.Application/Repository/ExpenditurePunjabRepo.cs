﻿using Dapper;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using PSPA.SDR.Application.Data;
using PSPA.SDR.Application.Models.DomainModel;
using PSPA.SDR.Application.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace PSPA.SDR.Application.Repository
{
    public class ExpenditurePunjabRepo : IExpenditurePunjabRepo
    {
        private readonly ApplicationDbContext _context;
        private readonly IConfiguration _configuration;
        public ExpenditurePunjabRepo(ApplicationDbContext context, IConfiguration configuration)
        {
            _context = context;
            _configuration = configuration;
        }

        public async Task<IEnumerable<ExpenditurePunjab>> FilterExpenditurePunjabByIdAsync(ExpenditurePunjabFilterViewModel model)
        {
            if (model.departmentId=="-1")
            {
                return await _context.expenditurePunjabs
              .Distinct().ToListAsync();
            }
            else
            {
                return await _context.expenditurePunjabs.
              Where(x => x.DeptName == model.departmentId && x.ProgramName == model.programId)
              .Distinct().ToListAsync();
            }
          
        }

        public async Task<List<SelectListItem>> GetDepartmentAsync()
        {
            string DbConnectionString = this._configuration.GetConnectionString("DefaultConnection");
            List<SelectListItem> selectListItem;
            using (IDbConnection db = new SqlConnection(DbConnectionString))
            {
                string readSp = "dbo.GetAllExpenditureDepartments";
                selectListItem = db.Query<SelectListItem>(readSp,  commandType: CommandType.StoredProcedure).ToList();
            }
            return selectListItem;
        }

        public async Task<IEnumerable<ExpenditurePunjab>> GetAllExpenditurePunjabAsync()
        {
            return await _context.expenditurePunjabs.ToListAsync();
        }

        public async Task<List<SelectListItem>> GetProgrammAsync()
        {
            string DbConnectionString = this._configuration.GetConnectionString("DefaultConnection");
            List<SelectListItem> selectListItem;
            using (IDbConnection db = new SqlConnection(DbConnectionString))
            {
                string readSp = "dbo.GetAllExpenditureProgramms";
                selectListItem =  db.Query<SelectListItem>(readSp, commandType: CommandType.StoredProcedure).ToList();
            }
            return selectListItem;
        }

        public async Task<IEnumerable<DepartmentWiseExpenseViewModel>> GetExpenditurePunjabDepartmentWiseAsync()
        {
            var departments = await _context.expenditurePunjabs.Select(x => x.DeptName).Distinct().ToListAsync();
            List<DepartmentWiseExpenseViewModel> departmentwiseList = new List<DepartmentWiseExpenseViewModel>();
            foreach(var department in departments)
            {
                var total = await _context.expenditurePunjabs.Where(x => x.DeptName == department).SumAsync(s => Convert.ToDouble(s.ExpTotal));

                var vm = new DepartmentWiseExpenseViewModel
                {
                    DepartmentName = department,
                    total = total
                };

                departmentwiseList.Add(vm);
            }
            return departmentwiseList;
        }

        public async Task<bool> SaveChangesAsync()
        {
            return (await _context.SaveChangesAsync() >= 0);
        }
    }
}
