﻿using Microsoft.AspNetCore.Mvc.Rendering;
using PSPA.SDR.Application.Models.DomainModel;
using PSPA.SDR.Application.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PSPA.SDR.Application.Repository
{
    public interface ISdrRepo
    {
        Task<bool> SaveChangesAsync();
        Task<IEnumerable<Sdr>> GetAllSdrAsync();
        Task<IEnumerable<DepartmentWiseExpenseViewModel>> GetExpenditurePunjabDepartmentWiseAsync();
        Task<IEnumerable<DepartmentWiseProgramViewModel>> FilterSdrByIdAsync(SdrFilterViewModel model);
        Task<IEnumerable<DepartmentWiseProgramViewModel>> FilterSdrByIdAndAllAsync(SdrFilterViewModel model);
        Task<List<DepartmentWiseProgramViewModel>> GetAllDepartmentsWiseAsync();
        Task<List<DepartmentWiseProgramViewModel>> GetAllDepartmentsRecords();
        Task<List<Sdr>> GetSearchResult(SdrFilterViewModel model);
        Task<ProgramWisestatsViewModel> ProgramWisestatsByIdAsync(SdrFilterViewModel model);
        Task<List<SelectListItem>> GetAllProgramsAsync();
        Task<List<SelectListItem>> GetAllDepartmentsAsync();
        Task<List<SelectListItem>> FilterProgrammByIdAsync(SdrFilterViewModel model);

    }
}
