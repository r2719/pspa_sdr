﻿using Microsoft.AspNetCore.Mvc.Rendering;
using PSPA.SDR.Application.Models.DomainModel;
using PSPA.SDR.Application.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PSPA.SDR.Application.Repository
{
    public interface IExpenditurePunjabRepo
    {
        Task<bool> SaveChangesAsync();
        Task<IEnumerable<ExpenditurePunjab>> GetAllExpenditurePunjabAsync();
        Task<IEnumerable<DepartmentWiseExpenseViewModel>> GetExpenditurePunjabDepartmentWiseAsync();
        Task<List<SelectListItem>> GetDepartmentAsync();
        Task<List<SelectListItem>> GetProgrammAsync();
        Task<IEnumerable<ExpenditurePunjab>> FilterExpenditurePunjabByIdAsync(ExpenditurePunjabFilterViewModel model);
        //Task<List<SelectListItem>> GetAllProgramsAsync();
        //Task<List<SelectListItem>> GetAllDepartmentsAsync();

    }
}
