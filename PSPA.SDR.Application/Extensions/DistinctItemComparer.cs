﻿using PSPA.SDR.Application.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading.Tasks;

namespace PSPA.SDR.Application.Extensions
{
    public class DistinctProgramComparer : IEqualityComparer<DepartmentWiseProgramViewModel>
    {

        public bool Equals(DepartmentWiseProgramViewModel x, DepartmentWiseProgramViewModel y)
        {
            return x.ProgramName == y.ProgramName;
        }

        public int GetHashCode( DepartmentWiseProgramViewModel obj)
        {
           return obj.ProgramName.GetHashCode();
        }
    }

    public class DistinctDistrictComparer : IEqualityComparer<DistrictWiseProgramsViewModel>
    {

        public bool Equals(DistrictWiseProgramsViewModel x, DistrictWiseProgramsViewModel y)
        {
            return x.District == y.District;
        }

        public int GetHashCode([DisallowNull] DistrictWiseProgramsViewModel obj)
        {
            return obj.District.GetHashCode();
        }
    } 
}
